package com.example.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

	@GetMapping("/sample/v1/test")
	public String testContent() {
		String msg = "Test Successfully done for version1";
		System.out.println(msg);
		return msg;
	}

	@GetMapping("/sample/v3/test")
	public String testContentNew() {
		String msg = "New API is created and Test Successfully done for version3";
		System.out.println(msg);
		return msg;
	}
	
}
